'use strict';

exports.init = function (app, config, auditLogger) {

    async function log(context) {
        var auditLog = {};
        var method = context.method;
        var req = context.req;

        auditLog.method = req.method;
        auditLog.url = req.originalUrl;
        auditLog.eventName = method.sharedClass.name;
        auditLog.subEventName = method.name;
		auditLog.useragent = req.headers['user-agent'];
        auditLog.arguments = {
            params: req.params,
            query: req.query,
            headers: req.headers,
            args: context.args
        };

        // remove request and response because of size and cicular references
        if (auditLog.arguments.args.req) {
            delete auditLog.arguments.args.req;
        }
        if (auditLog.arguments.args.res) {
            delete auditLog.arguments.args.res;
        }
        // fix authorizedRoles with for JSON invalid keys '$...'
        if (auditLog.arguments.args.options && auditLog.arguments.args.options.authorizedRoles) {
            auditLog.arguments.args.options.authorizedRoles = Object.keys(auditLog.arguments.args.options.authorizedRoles);
        }

        auditLog.result = Array.isArray(context.result) ?
        context.result.map(function (entry) {
            return entry.toJSON();
        }) : ((context.result && context.result.toJSON) ?
            context.result.toJSON() : {});
        auditLog.error = context.error || {};
        auditLog.status = context.error  ?
            (context.error.statusCode || context.error.status || context.res.statusCode) :
            (context.result && Object.keys(context.result) > 0 ? 200 : 204);
		
		if(req.headers && req.headers['x-forwarded-for']) {
			auditLog.loginid = req.headers.agentcode;
			if(req.headers['x-forwarded-for']){
				req.headers['x-forwarded-for'] = req.headers['x-forwarded-for'].split(':')[0];
			}
			auditLog.clientIp = req.headers['x-forwarded-for'];
			auditLog.token = req.headers.authorization;
		} else if (req.user && req.user.username) {
			auditLog.loginid = req.user.username;
			auditLog.clientIp = req.ip ||
                req._remoteAddress ||
                (req.connection && req.connection.remoteAddress) ||
                undefined;
			auditLog.token = req.headers.authorization;
		}
		
		if(!app.datasources.DB){
			app.datasources.DB = app.datasources.db;
		}
		
		if(auditLog.token && !auditLog.loginid) {
			auditLog.loginid = await getUserName(auditLog.token);					
			//console.log('auditLog.loginid :',auditLog.loginid);
		}
		
		async function getUserName(token){
			var username = '';
			var sql = "select username from AdminUser where id in (select top 1 userId from AdminAccessToken where id=@param1)";
			
			return new Promise((resolve,reject)=>{
				app.datasources.DB.connector.query(sql,[token],function(err,res) {
					
					if(err){
						console.error("Error Fetching adminuser for audit : ",err);
					}
					if(res.length>0){
						username=res[0].username;
					}
					//console.log('get username:',res);
					//console.log('get username:',username);
					return resolve(username);
				});
			});	
		}
		
		process.nextTick(function () {
                //auditLogger.info({'log': auditLog});
				var columns='(',params='(',i=1,values=[];
				
				for(var key in auditLog) {
					
					if(columns!='('){
						columns = columns+',';
						params = params+',';
					}
					columns = columns+key;
					params = params+'@param'+i;
					
					if(typeof auditLog[key]=='object'){
						auditLog[key] = JSON.stringify(auditLog[key]);
					}
					values.push(auditLog[key]);
					i++;
				}
				columns = columns+')';
				params = params+')';
				var sqlInsert = 'insert into auditlog '+columns+' values'+params;
				
				//console.log('sqlInsert :',sqlInsert);
				//console.log('values :',values);
				
				app.datasources.DB.connector.execute(sqlInsert, values, function(err,res) {
					if(err){
						console.error('Audit Log Error:',err)
					}
				});
				
        });
    }

    var models = app.models();
    models.forEach(function (Model) {

        Model.afterRemote('**', function (context, unused, next) {
            log(context);
            next();
        });
        Model.afterRemoteError('**', function (context, next) {
            log(context);
            next();
        });
    });
};
